<?php

class Router{
    /*Массив с маршрутами*/
    private $routes;

    /**
     * Router constructor.
     */
    public function __construct(){
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * Метод получения строки запроса
     * @return string
     */
    private function getURI(){
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Метод запуска FRONT CONTROLLER
     * Вызов соответсвующего URI контроллера
     * и соответсвующего экшена
     */
    public function run(){
        $uri = $this->getURI();

        //Перебор всех маршрутов
        foreach ($this->routes as $uriPattern => $path){

            //Проверка на наличие запроса в routes.php
            if(preg_match("~$uriPattern~", $uri)){

                //Внутренний путь
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                //Название action и controller
                $segments = explode('/', $internalRoute);
                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $actionName = 'action'. ucfirst(array_shift($segments));

                //Оставшиеся параметры из адресной строки
                $parameters = $segments;

                //Подключение нужного controller
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if(file_exists($controllerFile)){
                    include_once($controllerFile);
                }

                //Вызов controller и action
                $controllerObject = new $controllerName;
                $result = $controllerObject->$actionName($parameters);

                if ($result != null){
                    //Выход из перебора маршрутов
                    break;
                }
            }
        }
    }
}