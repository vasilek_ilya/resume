<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <title>Chat</title>
</head>
<body>
<div class="container">
    <h2 class="text-center">Чат - <?php echo $_SESSION['user']; ?></h2>
    <form method="post">
        <div class="row">
            <div class="row col-sm-9">
                <h6 class="col-form-label col-sm-2  ">Сообщение</h6>
                <textarea class="form-control col-sm-10" name="message" placeholder="Текст сообщения"></textarea>
            </div>
            <div class="col-sm-3 text-left">
                <input type="submit" class="btn btn-success" name="send" value="Отправить">
                <a href="/logout" class="btn btn-danger" role="button">Выход</a>
            </div>
        </div>
    </form>
    <div class="error-container mt-3 mx-auto">
        <?php if (is_array($errors)): ?>
            <?php foreach ($errors as $error): ?>
                <div class="alert alert-danger mb-0" role="alert" >
                    <?php echo $error; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <div class="message-container mt-3" style="filter: brightness(90%);">

    </div>
</div>
</body>
<script>
    function postRequest() {
        $.post(
            '/getMessage', {str: 'str'},
            function (data) {
                var messages = JSON.parse(data);
                $('.message-container').empty();
                for (var i = messages.length - 1; i >= 0; i--){
                    $('<div>', {
                        append: $('<h6>', {text: messages[i]['login']})
                            .add($('<div>', {text:messages[i]['message']}))})
                    .addClass('alert alert-' + messages[i]['status'])
                    .appendTo($('.message-container'));
                }
            }
        );
    };

    $(document).ready(function() {
        postRequest();
        setInterval(postRequest, 1000);
        }
    );
</script>
</body>
</html>