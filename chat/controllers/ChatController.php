<?php

class ChatController {

    /**
     * Функция записывает данные в сессию
     * и перенаправляет на страницу чата
     * @param $login
     */
    private function correctData($login) {
        $_SESSION['user'] = $login;
        header('Location: /messenger');
        die;
    }

    /**
     * Функция перенаправляет пользователя на страницу чата
     * если он уже авторизован и пытается перейти на други страницы
     */
    private function checkSession() {
        if (isset($_SESSION['user'])) {
            header('Location: /messenger');
            die;
        }
    }

    /**
     * Функция при переходе по ссылке '/'
     * @return bool
     */
    public function actionIndex() {

        $this->checkSession();

        require_once (ROOT . '/views/index.php');

        return true;
    }

    /**
     * Функция при переходе по ссылке '/login'
     * @return bool
     */
    public function actionLogin() {

        $this->checkSession();

        $errors = false;

        if (isset($_POST['submit'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            if (!User::checkLogin($login)) {
                $errors[] = 'Такого логина не существует!';
            }
            if (!User::checkPassword($login, $password)) {
                $errors[] = 'Неверный пароль!';
            }

            if ($errors == false) {
                $this->correctData($login);
            }
        }

        require_once (ROOT . '/views/login.php');

        return true;
    }

    /**
     * Функция при переходе по ссылке '/signUp'
     * @return bool
     */
    public function actionSignUp() {

        $this->checkSession();

        $errors = false;

        if (isset($_POST['submit'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            if ($login === '') {
                $errors[] = 'Логин не может быть пустым!';
            } elseif (User::checkLogin($login)) {
                $errors[] = 'Такой логин уже существует!';
            }
            if ($password === '') {
                $errors[] = 'Пароль не может быть пустым!';
            } elseif (strlen($password) < 4) {
                $errors[] = 'Пароль должен быть длинее 3 символов!';
            }

            if ($errors == false) {
                User::addUser($login, password_hash($password, PASSWORD_DEFAULT));
                $this->correctData($login);
            }
        }

        require_once (ROOT . '/views/signUp.php');

        return true;
    }

    /**
     * Функция при переходе по ссылке '/messenger'
     * @return bool
     */
    public function actionMessenger() {

        if (!isset($_SESSION['user'])){
            header('Location: /');
            die;
        }

        $errors = false;
        if (isset($_POST['send'])) {
            $message = $_POST['message'];

            if (trim($message) === '') {
                $errors[] = 'Введите сообщение!';
            }

            if ($errors == false) {
                Message::addMessage($message, $_SESSION['user']);
            }
        }

        require_once (ROOT . '/views/messenger.php');

        return true;
    }

    /**
     * Функция при переходе по ссылке '/logout'
     * Удаляет пользователя из сессии
     * @return bool
     */
    public function actionLogout() {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            header('Location: /');
            die;
        }

        return true;
    }

    /**
     * Функция принимающая POST запрос от скрипта
     * и возвращающая массив сообщений в формате JSON
     */
    public function actionGetMessage()
    {
        if (isset($_POST['str'])) {
            $messages = array();
            $messages = Message::getMessages();

            echo json_encode($messages);
            die;
        }
    }
}