<?php
//FRONT CONTROLLER

//Общие настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

//Начало сессии
session_start();

//Подключение файлов системы - установка корневого пути
define('ROOT', dirname(__FILE__));
//Автозагрузка классов
require_once(ROOT.'/components/Autoload.php');

//Вызов Router
$router = new Router();
$router->run();