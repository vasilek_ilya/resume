<?php

//Автоподключение созданных классов
spl_autoload_register(function ($className){
    //Массив с директориями откуда подключать классы
   $arrayPath = array(
       '/models/',
       '/components/'
   );

   foreach ($arrayPath as $path){
       $path = ROOT . $path . $className . '.php';
       if (is_file($path)){
           include_once ($path);
       }
   }
});

