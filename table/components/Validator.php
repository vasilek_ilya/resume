<?php

class Validator{

    /**
     * Валидация имени
     * @param $name
     * @return bool
     */
    public static function checkName($name){
        $pattern = "~^[a-zA-Zа-яА-Я]{1,}$~";
        if ($name && preg_match($pattern,$name)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Валидация даты
     * @param $date
     * @return bool
     */
    public static function checkDate($date){
        if($date){
            return true;
        } else {
            return false;
        }
    }
}