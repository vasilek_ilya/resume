<?php

class Twig{

    /**
     * Метод получения объекта Twig_Environment для рендеринга страницы
     * @return Twig_Environment
     */
    public static function getTwig(){
        require_once(ROOT . '/components/Twig/Autoloader.php');
        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem(ROOT . '/views');
        $twig = new Twig_Environment($loader);

        return $twig;
    }
}