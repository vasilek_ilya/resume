<?php

class Router{

    private $routes;

    /**
     * Router constructor.
     */
    public function __construct(){
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * Метод возвращающий URI строку
     * @return string
     */
    private function getURI(){
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Метод запуска Router
     */
    public function run(){
        //Получение строки заапроса
        $uri = $this->getURI();

        //Перебор всех путей из файла routes.php
        foreach ($this->routes as $uriPattern => $path){
            //Если путь из routes.php совпал с строкой запроса
            if(preg_match("~$uriPattern~", $uri)){
                //Определяем внутренний путь
                $internalPath = preg_replace("~$uriPattern~", $path, $uri);

                //Получение имени контроллера, экшена и парметров запроса
                $segments = explode('/', $internalPath);
                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;

                //Подключение контроллера
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)){
                    include_once ($controllerFile);
                }

                //Вызов контроллра и его экшена
                $controllerObject = new $controllerName;
                //Функция вызова метода $controllerObject->$actionName передавая массив $parameters
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                //Выход из перебора путей
                if ($result != null){
                    break;
                }
            }
        }
    }
}