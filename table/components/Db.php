<?php

class Db{

    /**
     * Метод подключения к базе данных через параметры в файле db_params.php
     * @return PDO
     */
    public static function getConnection(){
        $paramPath = ROOT . '/config/db_params.php';
        $params = include ($paramPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);
        $db->exec("set names utf8");

        return $db;
    }
}