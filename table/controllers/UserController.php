<?php

class UserController{

    /**
     * Метод удаления пользователя из БД по id
     * @param $id
     * @return bool
     */
    public function actionDelete($id){
        $id = intval($id);

        if ($id){
            User::deleteUserByID($id);
            echo true;
        }

        return true;
    }
}