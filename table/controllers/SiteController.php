<?php

class SiteController{
    /**
     * Очистка данных от посторонних символов
     * @param $value
     * @return string
     */
    private function cleanValue($value){
        //Удаление пробелов с начала и конца
        $value = trim($value);
        //Удаление экранированных символов
        $value = stripcslashes($value);
        //Удаление html u php тегов
        $value = strip_tags($value);
        //Отображение спец символов
        $value = htmlspecialchars($value);

        return $value;
    }

    /**
     * Обработка отправленных POST данных
     */
    private function checkPOST(){
        if (isset($_POST['add'])){
            $name = $this->cleanValue($_POST['nameAdd']);
            $date = $_POST['dateAdd'];

            User::addUser($name, $date);

            $referer = $_SERVER['HTTP_REFERER'];
            header("Location: $referer");

        }

        if (isset($_POST['edit'])){
            $name = $this->cleanValue($_POST['nameEdit']);
            $date = $_POST['dateEdit'];
            $id = $_POST['idEdit'];

            User::updateUserByID($id, $name, $date);

            $referer = $_SERVER['HTTP_REFERER'];
            header("Location: $referer");
        }
    }

    /**
     * Действие при перехода по ссылке "/"
     * @return bool
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function actionIndex(){
        $users = User::getUserList();

        $this->checkPOST();

        $twig = Twig::getTwig();
        echo $twig->render('index.php', array('users' => $users));

        return true;
    }

    /**
     * Действие при перехода по ссылке "/NameASC"
     * @return bool
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function actionNameASC(){
        $users = User::getUserListSortNameASC();

        $this->checkPOST();

        $twig = Twig::getTwig();
        echo $twig->render('index.php', array('users' => $users));

        return true;
    }

    /**
     * Действие при перехода по ссылке "/NameDESC"
     * @return bool
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     *
     */
    public function actionNameDESC(){
        $users = User::getUserListSortNameDESC();

        $this->checkPOST();

        $twig = Twig::getTwig();
        echo $twig->render('index.php', array('users' => $users));

        return true;
    }

    /**
     * Действие при перехода по ссылке "/DateASC"
     * @return bool
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function actionDateASC(){
        $users = User::getUserListSortDateASC();

        $this->checkPOST();

        $twig = Twig::getTwig();
        echo $twig->render('index.php', array('users' => $users));

        return true;
    }

    /**
     * Действие при перехода по ссылке "/DateDESC"
     * @return bool
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function actionDateDESC(){
        $users = User::getUserListSortDateDESC();

        $this->checkPOST();

        $twig = Twig::getTwig();
        echo $twig->render('index.php', array('users' => $users));

        return true;
    }
}