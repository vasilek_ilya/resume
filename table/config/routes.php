<?php

//Массив соответствий URL с внутренним путем
return array(
    'user/delete/([0-9]+)' => 'user/delete/$1', //UserController actionDelete
    'nameASC' => 'site/nameASC', //SiteController actionNameASC
    'nameDESC' => 'site/nameDESC', //SiteController actionNameDESC
    'dateASC' => 'site/dateASC', //SiteController actionDateASC
    'dateDESC' => 'site/dateDESC', //SiteController actionDateDESC
    '' => 'site/index' //SiteController actionIndex
);