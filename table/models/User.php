<?php

class User{
    const SHOW_BY_DEFAULT = 15;

    /**
     * Метод получения пользователя из БД по id
     * @param $id
     * @return array/false
     */
    public static function getUserByID($id){
        $db = Db::getConnection();
        $sql = 'SELECT * FROM users WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $user = $result->fetch();

        return $user;
    }

    /**
     * Метод удаления пользователя из БД по id
     * @param $id
     * @return bool|PDOStatement
     */
    public static function deleteUserByID($id){
        $db = Db::getConnection();
        $sql = 'DELETE FROM users WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->execute();

        return $result;
    }

    /**
     * Метод обновления данных пользователя в БД по id
     * @param $id
     * @param $name
     * @param $date
     * @return bool|PDOStatement
     */
    public static function updateUserByID($id, $name, $date){
        $db = Db::getConnection();
        $sql = 'UPDATE users SET name = :name , date = :date WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id',$id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':date', $date, PDO::PARAM_STR);

        $result->execute();
        return $result;
    }

    /**
     * Метод добавления пользователя в БД
     * @param $name
     * @param $date
     * @return bool|PDOStatement
     */
    public static function addUser($name, $date){
        $db = Db::getConnection();
        $sql = 'INSERT INTO users (id, name, date) VALUES (NULL, :name, :date)';

        $result = $db->prepare($sql);
        $result->bindParam(':name',$name, PDO::PARAM_STR);
        $result->bindParam(':date',$date, PDO::PARAM_STR);

        $result->execute();
        return $result;
    }

    /**
     * Метод выполняющий sql-запрос
     * @param $sql
     * @param $count
     * @return array
     */
    private static function getUserListBySQL($sql, $count){
        $db = Db::getConnection();

        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $userList = array();
        $i = 0;
        while ($row = $result->fetch()){
            $userList[$i]['id'] = $row['id'];
            $userList[$i]['name'] = $row['name'];
            $userList[$i]['date'] = $row['date'];
            $i++;
        }

        return $userList;
    }

    /**
     * Метод для получения последних пользователей
     * @param int $count
     * @return array
     */
    public static function getUserList($count = self::SHOW_BY_DEFAULT){
        $sql = 'SELECT * FROM users LIMIT :count';
        $userList = self::getUserListBySQL($sql, $count);

        return $userList;
    }

    /**
     * Метод для получения последних пользователей в порядке возрастания по имени
     * @param int $count
     * @return array
     */
    public static function getUserListSortNameASC($count = self::SHOW_BY_DEFAULT){
        $sql = 'SELECT * FROM users ORDER BY name ASC LIMIT :count';
        $userList = self::getUserListBySQL($sql, $count);

        return $userList;
    }

    /**
     * Метод для получения последних пользователей в порядке уюывания по имени
     * @param int $count
     * @return array
     */
    public static function getUserListSortNameDESC($count = self::SHOW_BY_DEFAULT){
        $sql = 'SELECT * FROM users ORDER BY name DESC LIMIT :count';
        $userList = self::getUserListBySQL($sql, $count);

        return $userList;
    }

    /**
     * Метод для получения последних пользователей в порядке возрастания по дате
     * @param int $count
     * @return array
     */
    public static function getUserListSortDateASC($count = self::SHOW_BY_DEFAULT){
        $sql = 'SELECT * FROM users ORDER BY date ASC LIMIT :count';
        $userList = self::getUserListBySQL($sql, $count);

        return $userList;
    }

    /**
     * Метод для получения последних пользователей в порядке убывания по дате
     * @param int $count
     * @return array
     */
    public static function getUserListSortDateDESC($count = self::SHOW_BY_DEFAULT){
        $sql = 'SELECT * FROM users ORDER BY date DESC LIMIT :count';
        $userList = self::getUserListBySQL($sql, $count);

        return $userList;
    }
}