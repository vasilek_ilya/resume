<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/template/css/style_index.css">
    <title>Table</title>
</head>
<body>
    <div class="content">
        <h2>Users</h2>

        <table>
            <tr>
                <th>
                    <span>ID</span>
                    <div id="AddUser">&#10010;</div>
                </th>
                <th>
                    <span>Name</span>
                    <div id="nameDESC"><a href="/nameDESC">&#9650;</a></div>
                    <div id="nameASC"><a href="/nameASC">&#9660;</a></div>
                </th>
                <th>
                    <span>Date</span>
                    <div id="dateDESC"><a href="/dateDESC">&#9650;</a></div>
                    <div id="dateASC"><a href="/dateASC">&#9660;</a></div>
                </th>
                <th></th>
            </tr>
            {%for user in users%}
            <tr id="tr{{user['id']}}">
                <td>{{user['id']}}</td>
                <td>{{user['name']}}</td>
                <td>{{user['date']}}</td>
                <td>
                    <div class="delete" data-id = "{{user['id']}}">&#10008;</div>
                    <div class="edit" data-id = "{{user['id']}}">&#9998;</div>
                </td>
            </tr>
            {%endfor%}
        </table>
    </div>
    <div class="dialog-form" id="dialog-form-add" style="display: none;">
        <div class="dialog-content">
            <div class="close" id="closeAdd">&#10008;</div>
            <form id="formAdd" method="post">
                <label for="name">Name</label>
                <input type="text" name="nameAdd" id="nameAdd" placeholder="name"><br>
                <label for="email">Date of Birth</label>
                <input type="date" name="dateAdd" id="dateAdd" ><br>
                <input type="submit" name="add" id="add" value="Add new user">
            </form>
        </div>
    </div>

    <div class="dialog-form" id="dialog-form-edit" style="display: none;">
        <div class="dialog-content">
            <div class="close" id="closeEdit">&#10008;</div>
            <form id="formEdit" method="post">
                <input type="text" name="idEdit" id="idEdit" readonly style="display: none">
                <label for="name">Name</label>
                <input type="text" name="nameEdit" id="nameEdit" placeholder="name"><br>
                <label for="email">Date of Birth</label>
                <input type="date" name="dateEdit" id="dateEdit" ><br>
                <input type="submit" name="edit" id="edit" value="Edit user">
            </form>
        </div>
    </div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    //Модальное окно с добавлением
    $("#AddUser").click(function () {
        $("#dialog-form-add").show();
    });
    $("#closeAdd").click(function () {
        $("#dialog-form-add").hide();
    });

    //Удаление пользователя
    $(".delete").click(function () {
        var id = $(this).attr("data-id");
        $.post("/user/delete/" + id, {}, function (data) {
            if (data) $("#tr" + id).remove();
        });
    });

    //Модальное окно с редактированием пользователя
    $(".edit").click(function () {
        $("#form-dialog-edit").show();
        var id = $(this).attr("data-id");
        var trs = $("#tr" + id).children();
        $("#idEdit").val(trs.eq(0).text());
        $("#nameEdit").val(trs.eq(1).text());
        $("#dateEdit").val(trs.eq(2).text());
        $("#dialog-form-edit").show();
    });
    $("#closeEdit").click(function () {
        $("#dialog-form-edit").hide();
    });


</script>
</html>

